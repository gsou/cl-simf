;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.
(in-package :cl-simf)

;; RW API
(defgeneric signal/read (sig tag)
  (:documentation "Perform a read on a signal."))
(defgeneric signal/write (sig tag value)
  (:documentation "Perform a write on a signal."))
(defgeneric interface-signal/read (iface))
(defgeneric interface-signal/write (iface value))
(defgeneric interface-signal/next (iface))

;; Binding API
(defgeneric signal/bind-in (sig)
  (:documentation "Create an input interface for this signal."))
(defgeneric signal/bind-out (sig)
  (:documentation "Create an output interface for this signal."))
(defgeneric signal/bind-message (sig)
  (:documentation "Create a message interface for this signal"))

;; Interfacing

(defclass interface-signal-read ()
  ((tag :initarg :tag
        :reader interface-signal/tag
        :documentation "Tag used by the signal to identify the interface that is trying to acces it.")
   (sig :initarg :sig
        :reader interface-signal/signal
        :documentation "The signal.")
   (events :initform nil
          :accessor interface-signal/events
          :documentation "The last events."))
  (:documentation "Class used to interface with a signal. Should not be created directly, but only via the signal bind function, which will determine a proper tag."))
(defmethod interface-signal/read ((iface interface-signal-read))
  "Perform a read of the signal."
  (signal/read (slot-value iface 'sig) (slot-value iface 'tag)))
(defmethod signal/bind-in ((sig interface-signal-read))
  "Create a new binding from the interface"
  (signal/bind-in (interface-signal/signal sig)))

(defclass interface-signal-write (interface-signal-read)
  ()
  (:documentation "Class used to interface with a signal. Should not be created directly, but only via the signal bind function, which will determine a proper tag."))
(defmethod interface-signal/write ((iface interface-signal-write) value)
  "Perform a write of the signal."
  (signal/write (slot-value iface 'sig) (slot-value iface 'tag) value))
(defun (setf interface-signal/read) (value iface)
  (interface-signal/write iface value))
(defmethod signal/bind-out ((sig interface-signal-write))
  "Create a new binding from the interface"
  (signal/bind-out (interface-signal/signal sig)))

(defclass interface-signal-message (interface-signal-write)
  ((mb :initform #'identity
       :documentation "Test function used to filter messages to be added to the fifo.")
   (fifo :initform (make-instance 'jpl-queues:lossy-bounded-fifo-queue :capacity 1024)
         :accessor interface-signal/fifo
         :documentation "The fifo containing the messages"))
  (:documentation "Class used to interface with a message passing signal. Should not be created directly, but only via the signal bind function, which will determine a proper tag"))
(defmethod interface-signal/next ((iface interface-signal-message))
  "Read the next value in the fifo"
  (and (not (jpl-queues:empty? (interface-signal/fifo iface)))
       (jpl-queues:dequeue (interface-signal/fifo iface))))
(defmethod signal/bind-message ((sig interface-signal-message))
  "Create a new binding from the interface"
  (signal/bind-message (interface-signal/signal sig)))

(defun interface-signal/clear (iface)
  "Clear the events of a signal"
  (setf (interface-signal/events iface) nil))
;; Signal types


(defclass signal-dir ()
  ((name :initarg :name
         :reader signal/name
         :documentation "The name of the signal, used when tracing or when displaying its value")
   (type :initarg :type
         :reader signal/type
         :documentation "The type of the signal contect.")
   (test :initform #'equal
         :initarg :test
         :reader signal/test
         :documentation "The test used to compare the signal value to its last value")
   (value :initarg :value
          :reader signal/value
          :documentation "The current value in the signal. The reader can be used to read without any sideeffects.")
   (ifaces-in :initform nil
              :documentation "A list of the interfaces connected in read mode")
   (ifaces-out :initform nil
               :documentation "A list of the interfaces connected in write mode")
   (ifaces-mess :initform nil
                :documentation "A list of the interfaces connected in message mode"))
  (:documentation "Directional signal. Only one writer is allowed."))
(defmethod signal/bind-in ((sig signal-dir))
  (let* ((tag (gensym))
         (iface (make-instance 'interface-signal-read :tag tag :sig sig)))
    (push iface (slot-value sig 'ifaces-in))
    iface))
(defmethod signal/bind-out ((sig signal-dir))
  (if (null (slot-value sig 'ifaces-out))
      (progn
        (let* ((tag (gensym))
               (iface (make-instance 'interface-signal-write :tag tag :sig sig)))
          (push iface (slot-value sig 'ifaces-out))
          iface))
      (error "The signal ~A is already bound !" (signal/name sig))))
;; TODO Add way to add initargs to interface
(defmethod signal/read ((sig signal-dir) tag)
  (declare (ignore tag))
  (slot-value sig 'value))
(defun interface-signal/throw-event (iface ev)
  "Add an event to an interface."
  (pushnew ev (interface-signal/events iface) :test #'eql))
(defun signal/throw-event (sig ev)
  "Throw an event to all the interfaces"
  (dolist (iface (slot-value sig 'ifaces-in)) (interface-signal/throw-event iface ev))
  (dolist (iface (slot-value sig 'ifaces-out)) (interface-signal/throw-event iface ev))
  ;; XXX Events for messages ?
  ;; (dolist (iface (slot-value sig 'ifaces-mess)) (interface-signal/throw-event iface ev))
  )
;; TODO Better event management
(defmethod signal/write ((sig signal-dir) tag value)
  ;; Detect events
  (let* ((v (slot-value sig 'value)))
    (unless (funcall (signal/test sig) value v) (signal/throw-event sig :changed))
    (when (and (datatype-bool value) (not (datatype-bool v))) (signal/throw-event sig :posedge))
    (when (and (not (datatype-bool value)) (datatype-bool v)) (signal/throw-event sig :negedge)))
  (setf (slot-value sig 'value) value))

(defclass signal-message (signal-dir)
  ((ifaces-mess :initform nil
                :documentation "A list of the interfaces connected in message mode"))
  (:documentation "This class adds messaging to the signal."))
(defmethod signal/bind-message ((sig signal-message))
  (let* ((tag (gensym))
         (iface (make-instance 'interface-signal-message :tag tag :sig sig)))
    (push iface (slot-value sig 'ifaces-mess))
    iface))
(defmethod signal/write ((sig signal-message) tag value)
  (call-next-method)
  ;; Pass messages
  (dolist (iface (slot-value sig 'ifaces-mess))
    (unless (eql tag (interface-signal/tag iface))
      (jpl-queues:enqueue value (interface-signal/fifo iface)))))

;; Resolver signal class
(defclass signal-resolver (signal-dir)
  ((resolver :initarg resolver
             :reader signal-resolver/resolver
             :documentation "The resolve function for the actual value.")
   (ifaces-value :initform (make-hash-table)
                 :accessor signal-resolver/ifaces-value
                 :documentation "The value written by each interface."))
  (:documentation "Signal where multiple writers are allowed. The RESOLVER function is used to select the actual value."))
(defun signal-resolver/compute-value (sig)
  "Compute the value and store it in the value slot."
  (let ((val (apply (signal-resolver/resolver sig)
         (loop for v being the hash-values in (signal-resolver/ifaces-value sig)
               collect v))))
    (setf (slot-value sig 'value) val)
    val))
(defmethod signal/write ((sig signal-resolver) tag value)
  ;; Set the interface value
  (setf (gethash tag (signal-resolver/ifaces-value sig)) value)
  ;; Detect events
  (let* ((v (slot-value sig 'value))
         (value (signal-resolver/compute-value sig)))
    (unless (funcall (signal/test sig) value v) (signal/throw-event sig :changed))
    (when (and (datatype-bool value) (not (datatype-bool v))) (signal/throw-event sig :posedge))
    (when (and (not (datatype-bool value)) (datatype-bool v)) (signal/throw-event sig :negedge))))
(defmethod signal/bind-out ((sig signal-resolver))
  (let* ((tag (gensym))
         (iface (make-instance 'interface-signal-write :tag tag :sig sig)))
    (push iface (slot-value sig 'ifaces-out))
    iface))

;; Higher level methods
(defgeneric interface-signal/eventp (iface ev)
  (:documentation "Detect whether the signal just threw the event"))
(defmethod interface-signal/eventp ((iface interface-signal-read) ev)
  (member ev (interface-signal/events iface)))

(defmethod signal/name ((iface interface-signal-read)) (signal/name (interface-signal/signal iface)))
(defmethod signal/type ((iface interface-signal-read)) (signal/type (interface-signal/signal iface)))
