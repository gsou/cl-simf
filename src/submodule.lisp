;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.
(in-package :cl-simf)

(defclass element-submodule (element-module environment-container)
  ((sub-desc :initform nil
             :documentation "A list of the submodule definition."))
  (:documentation "An element containing other elements. This class implements the contained elements
by registering them to this object, which is considered an environment. The submodule
takes its TIME and DT value from the parent environment. Every tick, the environment is stepped,
then the module's update function is called. To register the modules to this environment the method
ELEMENT/PREPARE is overwritten by subclasses. The submodule interface can be either attached to the
submodules or in the ELEMENT-SUBMODULE's update function."))
(defmethod environment/time ((env element-submodule))
  "Get the time from the parent environment"
  (environment/time (element/env env)))
(defmethod environment/dt ((env element-submodule))
  "Get the timestep from the parent environment"
  (environment/dt (element/env env)))
(defmethod element/tick :before ((el element-submodule))
  (environment/step el))
