;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.

(in-package :cl-simf)

(defgeneric environment/step (env &key &allow-other-keys)
  (:documentation "Perform a single simulation step in the environment. By default perform a single step. If STEPS is provided perform STEPS steps instead.
If TIME is provided, the simulation keeps running for a duration of TIME"))

(defclass environment-container ()
  ((elements :initform nil
             :accessor environment/elements
             :documentation "The collection of elements to update")
   (signals :initform (make-hash-table)
            :accessor environment/signals
            :documentation "The top level signals registered to this environment.")
   (interfaces :initform nil
            :accessor environment/interfaces
            :documentation "The top level interfaces registered to this environment."))
  (:documentation "A basic simulation environment that ticks the elements in its world"))

(defclass environment-runnable (environment-container)
  ((started :initform nil
            :documentation "Checks if the environment is started. Preparation will be performed at the first step if required."))
  (:documentation "A runnable environment is steppable and can be started on its own"))

(defclass environment-rt (environment-runnable)
  ((time :initform 0
         :type (INTEGER 0 4611686018427387903)
         :documentation "The current time")
   (last-time :initform 0
              :type (INTEGER 0 4611686018427387903)
              :documentation "The last time value")
   (start-time :initform 0
               :type (INTEGER 0 4611686018427387903)
               :documentation "The time value at the start of the simulation")))

(defclass environment (environment-runnable)
  ((time :initform 0.0d0
         :accessor environment/time
         :type double-float
         :documentation "The current time")
   (dt :initarg :dt
       :reader environment/dt
       :type double-float
       :documentation "The step time size"))
  (:documentation "ENVIRONMENT-HOST is an environment that is resposible for time tracking."))

(defmethod environment/time ((env environment-rt))
  "Returns the time in seconds"
  (/ (slot-value env 'time) internal-time-units-per-second))
(defmethod environment/dt ((env environment-rt))
  "Returns the dt in seconds"
  (/ (- (slot-value env 'time) (slot-value env 'last-time)) internal-time-units-per-second))

(defun environment/tick-elements (env)
  (dolist (el (environment/elements env))
    (element/tick el)))
(defmethod environment/step ((env environment-container) &key &allow-other-keys)
  (environment/tick-elements env))
(defmethod environment/step :before ((env environment-runnable) &key &allow-other-keys)
  (unless (slot-value env 'started)
    ;;(dolist (el (environment/elements env))
    ;;  (element/prepare el))
    (setf (slot-value env 'started) t)))

(defmethod environment/step ((env environment-rt) &key &allow-other-keys)
  (when (= 0 (slot-value env 'start-time))
    (setf (slot-value env 'start-time) (get-internal-real-time))
    (setf (slot-value env 'time) (get-internal-real-time)))
  (setf (slot-value env 'last-time) (slot-value env 'time))
  (setf (slot-value env 'time) (get-internal-real-time))
  (call-next-method))
(defmethod environment/step ((env environment) &key steps time &allow-other-keys)
  (if (or steps time)
      (loop with end-time = (if time (+ (environment/time env) time) 0)
            for i from 0
            while (and (or (null time) (< (environment/time env) end-time))
                       (or (null steps) (< i steps)))
            do (environment/step env))
      (progn
        (incf (environment/time env) (environment/dt env))
        (call-next-method))))

(defun environment/register-all (env &rest els)
  "Register all the given elements to the environment."
  (dolist (el els)
    (push el (environment/elements env))))
(defmacro environment/add-signals (env &rest sigs)
  "Register all the given signals to the environment."
  (if sigs
      (let ((e (gensym)))
        `(let ((,e ,env))
           ,@(loop for s in sigs
                   collect `(setf (gethash ',s (slot-value ,e 'signals)) ,s))))))

(defun environment/find-signal (find env &key (test #'eq) (key #'identity))
  "Find a signal in the registered signals."
  (let ((split (str:split *HIERARCHY-DIVIDER-CHAR* find :limit 2)))
    (if (cdr split)
        ;; TODO Check if the element is indeed an environment-container before doing that
        (environment/find-signal (cadr split) (environment/get-element-by-name env (car split)) :test test :key key)
        (loop for v being the hash-values in (slot-value env 'signals)
              when (funcall test find (funcall key v))
                do (return v)))))
(defun environment/get-element-by-type (env type)
  "Find the first registered element that is a subtype of the given type."
  (loop for v in (environment/elements env)
        do (when (typep v type) (return v))))

(defun environment/get-element-by-name (env name &key (test #'equal))
  (find name (environment/elements env) :key #'element/name :test test))

;; With simulation macro
(defun with-simulation-addsignal (env tf sig code)
  "(sig \"SIGNAL\" '(signal-dir bit) :bind-out t :bind-in t :trace t :value defval)"
  (destructuring-bind (sym name (sigtype innertype) &rest params) sig
    `(let ((,sym (make-instance ,sigtype :name (tracer-name ,name) :type ,innertype :value ,(if (member :value params) (getf params :value) `(default-value ,innertype)))))
         ,(let* ((wrap-iface-bind (cond
                                    ((equal :out (getf params :bind))
                                     (let ((ifacesym (gensym)))
                                       `(let ((,ifacesym (signal/bind-out ,sym)))
                                          (symbol-macrolet (( ,(intern (concatenate 'string "$" (symbol-name sym))) (interface-signal/read ,ifacesym)))
                                            ,@(if env `((push ,ifacesym (environment/interfaces ,env))))
                                            ,code))))
                                    ((equal :mess (getf params :bind))
                                     (let ((ifacesym (gensym)))
                                       `(let ((,ifacesym (signal/bind-message ,sym)))
                                          (symbol-macrolet (( ,(intern (concatenate 'string "$" (symbol-name sym))) (interface-signal/read ,ifacesym)))
                                            ,@(if env `((push ,ifacesym (environment/interfaces ,env))))
                                            ,code))))
                                    ((equal :in (getf params :bind))
                                     (let ((ifacesym (gensym)))
                                       `(let ((,ifacesym (signal/bind-in ,sym)))
                                          (symbol-macrolet (( ,(intern (concatenate 'string "$" (symbol-name sym))) (interface-signal/read ,ifacesym)))
                                            ,@(if env `((push ,ifacesym (environment/interfaces ,env))))
                                            ,code))))
                                    (t code)))
                 ;; FIXME Add back trace feature.
                 (wrap-trace-bind (if (getf params :trace)
                                      (if tf
                                          `(progn (element-tracer/add ,tf (signal/bind-in ,sym))
                                                  ,wrap-iface-bind)
                                          (error "Signal is trying to bind a :trace but the current context does not allow it"))
                                      wrap-iface-bind)))
            wrap-trace-bind))))
(defun with-simulation-defmodule-genbind (bind)
  (destructuring-bind (initarg dir sig) bind
      (list initarg (list (cond ((equal dir :in) 'signal/bind-in)
                                ((equal dir :out) 'signal/bind-out)
                                ((equal dir :mess) 'signal/bind-message)
                                ((equal dir :raw) 'identity)
                                (t (error "Invalid bind direction for genbind !")))
                          (if (keywordp sig)
                              (intern (symbol-name sig))
                              sig)))))
(defmacro make-module (env mod)
  (destructuring-bind (sym class binds &rest options) mod
    `(make-instance ,class
                    :env ,env
                    :name (tracer-name ,(symbol-name sym))
                    ,@(mapcan #'with-simulation-defmodule-genbind binds)
                    ,@options)))
(defun with-simulation-defmodule (env mod code)
    `(let ((,(car mod) (make-module ,env ,mod)))
       ,code))
(defmacro with-signals-top (signals env tf &body body)
  "Initialize the given signals in the body"
  (reduce (lambda (x acc) (with-simulation-addsignal env tf x acc))
          signals
          :from-end t
          :initial-value `(progn (environment/add-signals ,env ,@(mapcar #'car signals)) ,@body)))

(defun register-implicit-module-port-type (module port)
  (let* ((portid (if (keywordp port) (intern (symbol-name port) (symbol-package module)) port))
         (portdef (assoc portid (element-module/ports module)))
         (sigtype (first (third portdef)))
         (valtype (second (third portdef))))
    `(',sigtype ',valtype)))
(defun register-implicit-signal (signals module port signame)
  (let ((sigsymb (if (keywordp signame) (intern (symbol-name signame)) signame)))
    (if (or (not (keywordp signame)) (assoc sigsymb signals))
        ;; TODO Typechecking should be done here so that we only connect compatible signals
        signals
        ;; Add the signal
        (cons (list sigsymb (symbol-name signame) (register-implicit-module-port-type module port)) signals))))
(defun register-implicit-signals-module (signals module)
  "Add the SIGNALS implicitly defined in the MODULE definition"
  (let* ((modid (second module))
         (mod (if (symbolp modid) modid (second modid)))
         (binds (third module)))
    (reduce (lambda (bind acc) (register-implicit-signal acc mod (first bind) (third bind)))
            binds
            :from-end t
            :initial-value signals)))
(defmacro with-signals-autogen (signals modules env tf &body body)
  "Initialize the given signals. Signals used in the modules but not in the signal list are implicitly created if they are a keyword."
  `(with-signals-top
     ,(reduce (lambda (module acc) (register-implicit-signals-module acc module))
              modules
              :from-end t
              :initial-value signals)
     ,env ,tf ,@body))

(defmacro with-signals (signals &body body)
  `(with-signals-top ,signals nil nil ,@body))
(defmacro with-simulation (env-param
                           tracerpath
                           signals
                           modules
                           &body body)
  "Create a simple simulation."
  (let ((env (car env-param))
        (tf (car tracerpath))
        (el (gensym)))
    `(let ((,env (make-instance ,@(cdr env-param))))
       (with-element-tracer ,tracerpath ,env
         (with-signals-autogen ,signals ,modules ,env ,tf
           ;; (environment/add-signals ,env ,@(mapcar #'car signals))
           ,(reduce (lambda (x acc) (with-simulation-defmodule env x acc))
                    modules
                    :from-end t
                    :initial-value `(progn
                                      (environment/register-all ,env ,tf ,@(mapcar #'first modules))
                                      (dolist (,el (environment/elements ,env))
                                       (element/prepare ,el :tf ,tf))
                                      ,@body)))))))

(defclass monitor ()
  ((env
    :accessor monitor/env
    :type environment-runnable
    :documentation "The environment to monitor")
   (tracer
    :accessor monitor/tracer
    :type tracer
    :documentation "The tracer attached")
   (thread
    :accessor monitor/thread
    :type bt:thread
    :documentation "Thread ")
   (state
    :initarg :state
    :documentation "State of the simulation")
   (init-semaphore
    :initarg :init-semaphore
    :documentation "Init semaphore")
   (wait-semaphore
    :initarg :wait-semaphore
    :documentation "Wait semaphore")
   (update-semaphore
    :initarg :update-semaphore
    :documentation "Update semaphore"))
  (:documentation "Use to monitor and control a simulation running in another thread"))
(defgeneric monitor/wait-init (monitor)
  (:documentation "Wait for the simulation thread to mark the simulation as inited.
If run multiple times after the init is done, this function returns instantly. If run multiple times concurently, only a single call will return."))
(defgeneric monitor/resume (monitor state)
  (:documentation "Send a resume signal to the simulation."))
(defgeneric monitor/state (monitor)
  (:documentation "Retrieve the current state of the simulation."))

(defmethod monitor/wait-init ((monitor monitor))
  (with-slots (init-semaphore) monitor
    (if (eq t init-semaphore)
        t
        (progn (bt:wait-on-semaphore init-semaphore)
               (setf init-semaphore t)))))
(defmethod monitor/resume ((monitor monitor) st)
  (with-slots (wait-semaphore update-semaphore state) monitor
    (if (eq (monitor/state monitor) :wait)
        (progn
          (setf (car state) st)
          (bt:signal-semaphore wait-semaphore))
        (setf (car state) st))
    (bt:signal-semaphore update-semaphore)))
(defmethod monitor/state ((monitor monitor))
  (with-slots (state) monitor (car state)))
(defmacro monitor/on-update ((monitor &key (timeout 1)) &body body)
  `(bt:make-thread (lambda ()
                    (with-slots (update-semaphore) ,monitor
                      (loop
                        (progn
                          (bt:wait-on-semaphore update-semaphore :timeout ,timeout)
                          ,@body))))))
(defmacro with-simulation-monitor (env-param tracerpath signals modules &body body)
  "Launch a simulation in another thread and return the monitor instance."
  (with-gensyms (monitor init-semaphore wait-semaphore update-semaphore state)
    (let ((env (car env-param)))
      `(let* ((,state (list :wait))
              (,init-semaphore (bt:make-semaphore))
              (,wait-semaphore (bt:make-semaphore))
              (,update-semaphore (bt:make-semaphore))
              (,monitor (make-instance 'monitor :state ,state :init-semaphore ,init-semaphore :wait-semaphore ,wait-semaphore :update-semaphore ,update-semaphore)))
         (bt:make-thread (lambda ()
                           (with-simulation ,env-param ,tracerpath ,signals ,modules
                             (setf (monitor/env ,monitor) ,(car env-param))
                             (setf (monitor/tracer ,monitor) ,(car tracerpath))
                             ,@body
                             (flet ((next-state (st) (setf (car ,state) st) (bt:signal-semaphore ,update-semaphore)))
                               (bt:signal-semaphore ,init-semaphore)
                               (loop
                                 (match (car ,state)
                                   (:idle nil)
                                   (:wait (bt:wait-on-semaphore ,wait-semaphore))
                                   (:step (environment/step ,env) (next-state :wait))
                                   (:run (environment/step ,env))
                                   (:stop (return))
                                   ((list :until time) (if (>= (environment/time ,env) time) (next-state :wait) (environment/step ,env)))
                                   (_ (error "Invalid state ~A" (car ,state)))))))))
         ,monitor))))
