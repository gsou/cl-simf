;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.
(in-package :cl-simf)

(defclass element ()
  ((env :initarg :env :accessor element/env)
   (name :initarg :name :reader element/name
         :documentation "Symbol name for the element. Used to access modules by name in the environment"))
  (:documentation "An element is a class that is linked to the simulation environment. It ticks at the same time as the global simulation time."))

(defgeneric element/tick (el)
  (:documentation "Tick the element"))
(defgeneric element/prepare (el &key &allow-other-keys)
  (:documentation "Perform any tasks necessary before the simulation start"))
(defgeneric element-module/ports (elclass)
  (:documentation "List the bindable ports for the module"))

(defmethod element/tick ((el element)))
(defmethod element/prepare ((el element) &key &allow-other-keys))
(defun element/time (el)
  "Get the time of the environment attached to the element"
  (environment/time (element/env el)))
(defun element/dt (el)
  "Get the time of the environment attached to the element"
  (environment/dt (element/env el)))

;; Tracer element
(defclass element-tracer (element)
  ((tracer :initarg :tracer
           :reader element-tracer/tracer
           :documentation "The tracer")
   (downsample :initarg :downsample
               :initform 1
               :accessor element-tracer/downsample
               :documentation "Reduce the frequency of the updates to the tracer")
   (continuous :initarg :continuous
               :initform nil
               :accessor element-tracer/continuous
               :documentation "Always push the values, not only on changes")
   (step-id :initform 0
               :accessor element-tracer/step-id)
   (sigs :initform nil
         :reader element-tracer/sigs
          :documentation "The signals reader interface to be traced"))
  (:documentation "Connects signals to a tracer and update every tick"))
(defun element-tracer/add (el s)
  (push s (slot-value el 'sigs)))
(defmethod element/tick ((el element-tracer))
  (incf (element-tracer/step-id el))
  (when (= 0 (mod (element-tracer/step-id el)
                  (element-tracer/downsample el)))
    (let ((changes nil))
      (dolist (iface (element-tracer/sigs el))
        (let ((s (interface-signal/signal iface)))
          (when (or (element-tracer/continuous el) (interface-signal/eventp iface :changed)) ;; TODO Better event passing
            (setf changes t)
            (tracer/update (element-tracer/tracer el) (environment/time (element/env el)) (signal/name s) (signal/value s))
            (interface-signal/clear iface))))
      (when changes (tracer/draw (element-tracer/tracer el) (environment/time (element/env el)))))))
(defmethod element/prepare ((el element-tracer) &key &allow-other-keys)
  (dolist (s (mapcar #'interface-signal/signal (element-tracer/sigs el)))
    (tracer/register (element-tracer/tracer el) (signal/name s) (signal/type s)))
  (tracer/prepare (element-tracer/tracer el)))
(defmacro with-element-tracer ((var &rest trdef) env &body body)
  (let ((tracer (gensym)))
    `(with-tracer ,(cons tracer trdef)
       (let ((,var (make-instance 'element-tracer :tracer ,tracer :name ,(symbol-name var) :env ,env)))
         ,@body))))

;; Module
(defclass element-module (element)
  ((ports-desc :initform nil
               :accessor element-module/ports-desc
               :documentation "A list of the ports definition. This list contains elements of the form ( iface-name :in|:out signal-type )."))
  (:documentation "A module is an element that contain interface ports."))
(defmethod element/tick :after ((el element-module))
  "Clear the events from the interfaces."
  (dolist (desc (element-module/ports-desc el))
    ;; Get the slot name
    (let ((sym (car desc)))
      (interface-signal/clear (slot-value el sym)))))

;; Helper macros

(defun always-condition (c)
  "Produce the condition of a sensitivity list entry."
  (cond
    ((symbolp c) `(interface-signal/eventp ,c :changed))
    ((and (listp c) (= 2 (length c)) (equalp "posedge" (symbol-name (first c))))
     `(interface-signal/eventp ,(second c) :posedge))
    ((and (listp c) (= 2 (length c)) (equalp "negedge" (symbol-name (first c))))
     `(interface-signal/eventp ,(second c) :negedge))
    (t (error "Invalid sensitivity entry ~S" c))))
(defmacro always ( sens &body body )
  "Conditional execution on the status of the given signals."
  `(when (or ,@(mapcar #'always-condition sens))
     ,@body))
(defmacro on-message ( (bind iface) &body body)
  "Run the BODY as long as there are more message in iface"
  `(loop for ,bind = (interface-signal/next ,iface)
         while ,bind
         do (progn ,@body)))

