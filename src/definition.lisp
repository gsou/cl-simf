;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.
(in-package :cl-simf)

(defun defmodule-class-genslot (arg)
  "Generate an interface slot named NAME with direction DIR of type TYP"
  (destructuring-bind (name dir typ) arg
    (declare (ignore dir typ))
    `(,name :initform nil :initarg ,(intern (symbol-name name) 'keyword))))
(defun defmodule-class-gensymbolmacro (arg)
  "Generate a symbol macro to access the interface content"
  (destructuring-bind (name dir typ) arg
    (declare (ignore dir typ))
    `(,(intern (concatenate 'string "$" (symbol-name name)))
      (interface-signal/read ,name))))
(defun defmodule-module-genslot (mod)
  "Generate a module slot named NAME. The initform is nil since the allocation occur in the
ELEMENT/PREPARE method. "
  `(,(car mod) :initform nil))
(defun defmodule-defmodule (env mod tf)
  (let ((sym (car mod)))
    `(progn
       (setf ,sym (make-module ,env ,mod))
       (element/prepare ,sym :tf ,tf)
       )))
(defmacro defsubmodule-class (name subclasses ports extra-slots tickfn signaldef moduledef &rest options)
  "Define a subclass of element-submodule. If no submodules are specified i.e. MODULEDEF is nil, then a
subclass of element-module is created instead."
  (defmethod element-module/ports ((el (eql name)))
    (declare (ignore el))
    ports)
    `(progn
       (defclass ,name ,(cons (if moduledef 'element-submodule 'element-module) subclasses)
         ,(concatenate 'list
           (mapcar #'defmodule-class-genslot ports)
           (mapcar #'defmodule-module-genslot moduledef)
           `((ports-desc :initform (quote ,ports)
                         :accessor element-module/ports-desc)
             ,@(if moduledef
                   `((sub-desc :initform (quote ,moduledef)))))
           extra-slots)
         ,@options)
       (defmethod element/tick ((el ,name))
         (with-slots ,(mapcar #'first ports) el
           (symbol-macrolet
               ,(mapcar #'defmodule-class-gensymbolmacro ports)
             ,@tickfn)))
       ,@(if moduledef
             `((defmethod element/prepare ((el ,name) &key tf &allow-other-keys)
                 ;; XXX should we bind extra slots here ?
                 ;; Define the submodule creation routine
                 (with-tracer-scope (slot-value el 'name)
                   (with-slots ,(mapcar #'car ports) el
                     ;; Bind the module names
                     (with-slots ,(mapcar #'car moduledef) el
                       (with-signals-autogen ,signaldef ,moduledef el tf
                         ;; (environment/add-signals el ,@(mapcar #'car signaldef))
                         ,@(mapcar (lambda (mod) (defmodule-defmodule 'el mod 'tf)) moduledef)
                         (environment/register-all el ,@(mapcar #'first moduledef))
                         )))))))))
(defmacro defmodule-class (name subclasses ports extra-slots tickfn &rest options)
  "Define a subclass of element-module."
  `(defsubmodule-class ,name ,subclasses ,ports ,extra-slots ,tickfn () () ,@options))
(defmacro defsubmodule (name ports tickfn signaldef moduledef &rest options)
  `(defsubmodule-class ,name () ,ports () ,tickfn ,signaldef ,moduledef ,@options))
(defmacro defmodule (name ports tickfn &rest options)
  "Define a new module."
  `(defmodule-class ,name () ,ports () ,tickfn ,@options))
