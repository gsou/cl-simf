;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.

(in-package :cl-simf)

(defmodule-class generator ()
  ((value :out (signal-dir t)))
  ((function :initarg :function))
  ((setf $value (funcall (slot-value el 'function) (element/time el))))
  (:documentation "Produce a time-dependant value"))

(defmodule-class comb1 ()
  ((value :out (signal-dir t))
   (in :in (signal-dir t)))
  ((function :initarg :function)
   (time :initarg :time :initform nil
         :documentation "If this is non-nil, add the current time as the first argument to the function."))
  ((if (slot-value el 'time)
       (setf $value (funcall (slot-value el 'function) (element/time el) $in))
       (setf $value (funcall (slot-value el 'function) $in))))
  (:documentation "Combine a signal with an operation"))
(defmodule-class comb2 ()
  ((value :out (signal-dir t))
   (in1 :in (signal-dir t))
   (in2 :in (signal-dir t)))
  ((function :initarg :function)
   (time :initarg :time :initform nil
         :documentation "If this is non-nil, add the current time as the first argument to the function."))
  ((if (slot-value el 'time)
       (setf $value (funcall (slot-value el 'function) (element/time el) $in1 $in2))
       (setf $value (funcall (slot-value el 'function) $in1 $in2))))
  (:documentation "Combine two signals with an operation"))
