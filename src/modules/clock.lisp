;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.

(in-package :cl-simf)

(defmodule-class clock ()
  ((clk :out (signal-dir bit)))
  ((period :initarg :period :reader clock/period))
  ((setf $clk (if (>= (mod (environment/time (element/env el)) (clock/period el)) (/ (clock/period el) 2)) 1 0)))
  (:documentation "A clock will oscillate its output at the given frequency"))
