;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.

(in-package :cl-simf)

(defmodule-class replay-candump ()
  ((can :mess (signal-message can)))
  ((path :initarg :path :accessor replay-candump/path
         :documentation "File path containing the dump")
   (messages :initform (make-array 0 :fill-pointer 0)
             :accessor replay-candump/messages)
   (times :initform (make-array 0 :fill-pointer 0)
             :accessor replay-candump/times)
   (ix :initform 0 :accessor replay-candump/ix))
  ((let ((time-offset (elt (replay-candump/times el) 0)))
     (loop while (< (replay-candump/ix el) (length (replay-candump/times el)))
           for time = (elt (replay-candump/times el) (replay-candump/ix el))
           while (>= (element/time el) (- time time-offset))
           do (progn
                (setf $can (elt (replay-candump/messages el) (replay-candump/ix el)))
                (incf (replay-candump/ix el))))))
  (:documentation "Replays the messages logged by a candump process"))

(defun parse-candump-line (line)
  "Split a line from the output of candump into a time message, along with its timestamp"
  (let ((parts (cl-ppcre:split "\\s+" line)))
    (values
     (make-can :id (parse-integer (nth 3 parts) :radix 16)
               :data (coerce (mapcar #'(lambda (x) (parse-integer x :radix 16))
                                     (subseq parts 5))
                             'vector))
     (parse-float:parse-float (nth 1 parts) :start 1 :end (1- (length (nth 1 parts)))
                              :type 'double-float))))

(defmethod element/prepare ((el replay-candump) &key &allow-other-keys)
  (with-open-file (stream (replay-candump/path el))
    (loop for line = (read-line stream nil)
          while line
          do (multiple-value-bind (can time) (parse-candump-line line)
               (vector-push-extend time (replay-candump/times el))
               (vector-push-extend can (replay-candump/messages el))))))
