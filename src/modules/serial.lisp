;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.

(in-package :cl-simf)

(defmodule-class uart ()
  ((uart :mess (signal-message (integer 0 255)))
   (rx :in (signal-dir bit))
   (tx :out (signal-dir bit))
   (clk :in (signal-dir bit)))
  ((data :initform (make-instance 'jpl-queues:unbounded-fifo-queue)
         :documentation "Fifo for the TX interface")
   (tx-state :initform 10)
   (tx-data :initform 0)
   (rx-data :initform 0)
   (rx-state :initform 0))
  (;; Transmitter interface
   (on-message (m uart)
     (with-slots (data) el
       (jpl-queues:enqueue m data)))
   (always ((posedge clk))
     (with-slots (tx-state tx-data data rx-data rx-state) el
       ;; Transmitter statemachine
       (cond
         ((and (= 10 tx-state) (not (jpl-queues:empty? data)))
          (setf tx-state 0)
          (setf tx-data (jpl-queues:dequeue data)))
         ;; Stop bit
         ((= tx-state 9) (setf $tx 1) (incf tx-state))
         ((= tx-state 10) (setf $tx 1))
         ;; Start bit
         ((= tx-state 0) (setf $tx 0) (incf tx-state))
         ;; Data
         (t (setf $tx (if (= 0 (logand tx-data (ash 1 (1- tx-state)))) 0 1))
            (incf tx-state)))

       ;; Receiver statemachine
       (case rx-state
         ;; Wait for start
         (0 (when (= 0 $rx)
              (setf rx-state 1)
              (setf rx-data 0)))
         ;; Last bit
         (8 (setf rx-data (logior rx-data (ash $rx 7))
                  $uart rx-data)
            (incf rx-state))
         ;; Wait for stop bit
         (9 (when $rx (setf rx-state 0)))
         (t (setf rx-data (logior rx-data (ash $rx (1- rx-state))))
            (incf rx-state))))))
  (:documentation "A full-duplex 8-bit uart transmitter/receiver. Converts a SIGNAL-MESSAGE of the received/transmitted bytes to a RX and a TX bit signals."))
