;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.

(defpackage :cl-simf
  (:use :cl :alexandria :trivia)
  (:nicknames :simf :f)
  (:export
   #:with-tracer
   #:with-element-tracer
   #:defmodule
   #:defmodule-class
   #:always
   #:signal/read
   #:signal/write
   #:interface-signal/read
   #:interface-signal/write
   #:signal-dir
   #:element-tracer/add
   #:environment/register-all
   #:signal/bind-in
   #:signal/bind-out
   #:environment
   #:environment/step
   #:interface-signal/eventp
   #:with-simulation
   #:tracer/draw
   #:environment/time
   #:clock
   #:tracer-pp
   #:tracer-pp/get
   #:pp-entry/time
   #:pp-entry/value
   #:tracer/type-map
   #:on-message
   #:can-id
   #:can-data
   #:can
   #:signal-message
   #:can-setf-extract
   #:can-extract
   #:replay-candump
   #:plot-all
   #:comb2
   #:comb1
   #:element/name
   #:element
   #:element-module
   #:signal-resolver
   #:logic-resolve
   #:uart
   #:defsubmodule
   #:defsubmodule-class
   #:with-signals
   #:signal/name
   #:tracer/tracing-p
   #:environment/find-signal
   #:signal/type
   #:pp-entry
   #:environment/get-element-by-type
   #:element-tracer
   #:element-tracer/tracer
   #:element-submodule
   #:interface-signal-read
   #:interface-signal-write
   #:interface-signal-message
   #:with-simulation-monitor
   #:monitor
   #:monitor/env
   #:monitor/tracer
   #:monitor/thread
   #:monitor/wait-init
   #:monitor/resume
   #:monitor/state
   #:monitor/on-update
   #:file-tracer/path
   #:environment/interfaces
   #:environment/signals))
