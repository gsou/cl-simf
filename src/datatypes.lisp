;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.

(in-package :cl-simf)


;; Logic type

(defun logicp (obj)
  "Checks whether the object is a valid logic value."
  (or (eq obj t) ;; Truth value
      (null obj) ;; False value
      (eql :x obj) ;; Collision value
      (eql :z obj))) ;; Threestate value

(deftype logic () `(satisfies logicp))
(defun logic-resolve (&rest states)
  (cond
    ((find :x states) :x)
    ((and (member nil states) (member t states)) :x)
    ((member t states) t)
    ((member nil states) nil)
    (t :z)))

;; bit type

;; TODO Vector types

;; Message types

;; CAN Messages
(defstruct can id data)
(defun extract-i16 (vec offset &key be)
  "Extract a 16 bit signed integer in the byte vector VEC starting at OFFSET. By default the conversion is little-endian. if :BE T is specified, it is big-endian instead."
  (let* ((byte1 (elt vec offset))
         (byte2 (elt vec (1+ offset)))
         (lsb (if be byte2 byte1))
         (msb (if be byte1 byte2)))
    (- (logior (logand #xFF lsb) (ash (logand #x7F msb) 8))
       (if (> (logand #x80 msb) 0) 32768 0))))
(defun extract-u16 (vec offset &key be)
  "Extract a 16 bit unsigned integer in the byte vector VEC starting at OFFSET. By default the conversion is little-endian. if :BE T is specified, it is big-endian instead."
  (let* ((byte1 (elt vec offset))
         (byte2 (elt vec (1+ offset)))
         (lsb (if be byte2 byte1))
         (msb (if be byte1 byte2)))
    (logior (logand #xFF lsb) (ash (logand #xFF msb) 8))))
(defun can-extract (can &key id (mask #xFFFFFFFF) (pos 0) (width 1) signed be (offset 0) (scale 1) (post #'identity))
  "Extract numerical data from the can message CAN. Filtering on the can-id is done by ID = can-id & MASK. The number is a WIDTH byte number starting at POS. It is SIGNED if SIGNED is set.
If BE, the conversion is big-endian. Once the number is extracted, it is transformed by (funcall POST (* SCALE (+ OFFSET number)))."
  ; Filtering
  (if (= (logand id mask) (logand mask (can-id can)))
      (funcall post (* scale (+ offset
                                (let ((u 0))
                                  (loop for ix from 0 below width
                                        do (setf (ldb (byte 8 (if be (* 8 (1- (- width ix))) (* 8 ix))) u)
                                                 (elt (can-data can) (+ pos ix))))
                                  (if signed
                                      (let ((umask (1- (expt 2 (1- (* 8 width))))))
                                        (if (logbitp (1- (* 8 width)) u)
                                            (- (logand umask u) (expt 2 (1- (* 8 width))))
                                            (logand umask u)))
                                      u)))))))
(defmacro can-setf-extract (can &body binds)
  "Extract the values from a can message to the given place."
  (with-gensyms (c)
    `(let ((,c ,can))
       ,@(loop for b in binds
               collect (with-gensyms (s) `(let ((,s (can-extract ,c ,@(cdr b))))
                                            (if ,s (setf ,(first b) ,s))))))))

;; Functions on datatypes
(defun default-value (typ)
  "Return a suitable default value for the given type."
  (cond
    ((subtypep typ 'integer) 0)
    ((subtypep typ 'float) 0e0)
    ((subtypep typ 'single-float) 0e0)
    ((subtypep typ 'double-float) 0d0)
    ((subtypep typ 'number) 0)
    ((eql typ 'logic) :x)
    (t (error "Can't find a suitable default value for type ~A" typ))))

(defun datatype-format (typ val)
  "Format the value VAL with the given type TYP for display or tracing as text."
  (unless (typep val typ) (error "The given value ~A can't be formatted as type ~A" val typ))
  (match typ
    ('logic (format nil "~A" val))
    ('bit (format nil "~A" val))
    ((guard _ (subtypep typ 'integer)) (format nil "~A" val))
    ((guard _ (subtypep typ 'real)) (format nil "~,16,,,,,'eg" val))
    (t (error "Usupported type to format"))))


(defun datatype-bool (dat)
  "Test the datatype for truth"
  (cond
    ((numberp dat) (/= dat 0))
    (t dat)))

;; VCD Specific functions
(defun datatype-vcd-type (typ)
  (match typ
    ('logic (values "wire" 1))
    ('bit (values "wire" 1))
    ((list 'integer 0 max) (values "integer" max))
    ((guard _ (subtypep typ 'integer)) (values "integer" "None"))
    ((guard _ (subtypep typ 'real)) (values "real" "None"))
    (t (error "Type is not compatible with VCD"))))
(defun logic-to-vcd-string (c)
  (declare (type logic c))
  (cond
    ((null c) "0")
    ((eql :x c) "x")
    ((eql :z c) "z")
    ((eql t c) "1")))
(defun datatype-vcd-format (typ val)
  (match typ
    ('logic (logic-to-vcd-string val))
    ('bit (format nil "~A" val))
    ;; ((list 'integer 0 max) (format nil "b~b " val))
    ((guard _ (subtypep typ 'integer)) (format nil "b~b " val))
    ((guard _ (subtypep typ 'real)) (format nil "r~,16,,,,,'eg " val))
 (t (error "Type is not compatible with VCD"))))
