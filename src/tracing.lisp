;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.

(in-package :cl-simf)

;; TODO Add protection for functions when prepare has been called

(defparameter *HIERARCHY-DIVIDER-CHAR* #\/
  "The list of characters considered as a separation marker")
(defparameter *HIERARCHY-SCOPE* nil
  "The current scope of the ")
;; (define-symbol-macro hierarchy-divider-char (first *hierarchy-divider-chars*))
(defmacro with-tracer-scope (name &body body)
  `(unwind-protect
       (progn
         (push ,(car (last (uiop:split-string name :max 2 :separator (list *hierarchy-divider-char*)))) *HIERARCHY-SCOPE*)
         (progn ,@body))
    (pop *HIERARCHY-SCOPE*)))
(defun tracer-name (name)
  (format nil "~{~A~^/~}" (reverse (cons name *HIERARCHY-SCOPE*))))


;; Tracing

(defclass tracer ()
  ;; XXX Does this create a new hash-table every time ?
  ((type-map :initform (make-hash-table :test #'equal) :reader tracer/type-map)
   (locked :initform nil
           :documentation "Mark if the tracer is currently locked. If it is, it is not possible to register anymore.
Also, it is not possible to update the tracer until it is locked. To lock the tracer, call tracer/prepare"))
  (:documentation "This class defines the basis of the tracing class"))

(defgeneric tracer/oncreate (tracer) (:documentation "Tracer operations directly done after the creation of the tracer."))
(defgeneric tracer/ondestroy (tracer) (:documentation "Tracer operations done after the tracer is done being used."))
(defgeneric tracer/register (tracer key type) (:documentation "Register a key composed of the string KEY into the tracer TRACER.
TYPE is the type of the keylist. The KEY"))
(defgeneric tracer/prepare (tracer) (:documentation "Perform the operations required when the system is fully registered"))
(defgeneric tracer/update (tracer time key value) (:documentation "Update the value (and draw for some formats)"))
(defgeneric tracer/draw (tracer time) (:documentation "Draw the tracer for the current time tick"))

(defmethod tracer/oncreate ((tracer tracer)))
(defmethod tracer/ondestroy ((tracer tracer)))
(defmethod tracer/register ((tracer tracer) (key string) type)
  "Add the KEY/TYPE pair to the TRACER type-map."
  (setf (gethash key (slot-value tracer 'type-map)) type))
(defmethod tracer/register :before ((tracer tracer) (key string) type)
  (when (slot-value tracer 'locked) (error "Tracer is locked !")))
(defmethod tracer/prepare ((tracer tracer)))
(defmethod tracer/prepare :after ((tracer tracer))
  (setf (slot-value tracer 'locked) t))
(defmethod tracer/update ((tracer tracer) time (key string) value))
(defmethod tracer/update :before ((tracer tracer) time (key string) value)
  (declare (ignore time key value))
  (unless (slot-value tracer 'locked) (error "Tracer is not ready for update !")))
(defmethod tracer/draw ((tracer tracer) time))

(defun tracer/tracing-p (tr sym &key (key #'identity))
  "Return if the tracer TR has registered the symbol SYM"
  (gethash (funcall key sym) (tracer/type-map tr)))

;; Value tracer (stores the most recent value of each key)
(defclass tracer-value (tracer)
  ((value-map :initform (make-hash-table :test #'equal)
             :documentation "The hash-map containting the latest value for each key"))
  (:documentation "Tracer that stores the latest value of each key"))
(defmethod tracer/register :before ((tracer tracer-value) (key string) type)
  (setf (gethash key (slot-value tracer 'value-map)) (default-value type)))
(defmethod tracer/update :before ((tracer tracer-value) time (key string) value)
  (declare (ignore time))
  ;; TODO Check type ?
  (setf (gethash key (slot-value tracer 'value-map)) value))

;; Post processing tracer, stores every changes in memory
(defclass pp-entry ()
  ((time :initform (make-array 0 :fill-pointer 0) :accessor pp-entry/time)
   (value :initform (make-array 0 :fill-pointer 0) :accessor pp-entry/value))
  (:documentation "An entry for the postprocessing tracer."))
(defun pp-entry/clear (pp)
  (setf (slot-value pp 'time) (make-array 0 :fill-pointer 0))
  (setf (slot-value pp 'value) (make-array 0 :fill-pointer 0)))
(defun pp-entry/push (pp time value)
  "Push a new point to the post-processing entry."
  (vector-push-extend time (pp-entry/time pp))
  (vector-push-extend value (pp-entry/value pp)))
(defun pp-entry/xrange (pp)
  "Get the timerange of the entry. Return the first and last value in the timevector"
  (let ((tv (pp-entry/time pp)))
    (values
     (aref tv (1- (length tv)))
     (aref tv 0))))
(defun pp-entry/sample (pp time)
  "Sample from the pp-entry. Returns the first value that is >= the TIME.
The time is provided as a second value and the index as a third."
  ;;TODO Use a better algo there, this will be terribly slow
  (loop for i from 0 to (1- (length (pp-entry/time pp)))
        for at = (aref (pp-entry/time pp) i)
        when (>= at time)
        do (return (values (aref (pp-entry/value pp) i) at i))))

(defclass tracer-pp (tracer)
  ((vector-map :initform (make-hash-table :test #'equal)
               :accessor tracer-pp/vector-map))
  (:documentation "Tracer that stores every change in memory, for further analysis"))
(defmethod tracer/register ((tracer tracer-pp) key type)
  (call-next-method)
  (setf (gethash key (tracer-pp/vector-map tracer)) (make-instance 'pp-entry)))
(defmethod tracer/update ((tracer tracer-pp) time key value)
  (pp-entry/push (gethash key (tracer-pp/vector-map tracer)) time value))
(defgeneric tracer-pp/get (key tracer)
  (:documentation "Get a post processing entry for the given key"))
(defmethod tracer-pp/get (key (tracer tracer-pp))
  (gethash key (tracer-pp/vector-map tracer)))

;; File tracing

(defparameter *file-tracer-list* nil
  "Assoc list of the tracers. The key is the string representing the file extension. The value is the symbol representing the class.")

(defclass file-tracer (tracer)
  ((path :initarg :path
         :documentation "Represents the file path where to write the traces")
   (file :initarg :file
         :documentation "The file handle of the file-tracer."))
  (:documentation "Tracer that writes traces to a file"))
(defun file-tracer/path (tracer) (slot-value tracer 'path))
(defun file-tracer/format (tracer &rest args)
  "Format write to the tracer."
  (apply #'format (slot-value tracer 'file) args))

(defmacro define-file-tracer (ext subclasses &body args)
  "Define a file trace for the given extension EXT+."
  (let* ((extstr (symbol-name ext))
        (s (intern (concatenate 'string "FILE-TRACER-" extstr))))
    `(progn
       (if (assoc ,extstr *file-tracer-list* :test #'equalp)
           ;; TODO Fix this
           nil
           ;; (setf (cdr (assoc ,ext *file-tracer-list* :test #'equalp)) (quote ,s))
           (push (cons ,extstr (quote ,s)) *file-tracer-list*))
       (defclass ,s
           ,(cons 'file-tracer subclasses)
         ,@args))))

(defun get-file-tracer-for-ext (ext)
  "Get the file tracer attached to the given file extension EXT."
  (cdr (assoc ext *file-tracer-list* :test #'equalp)))

(defgeneric get-file-tracer-element-type (ext)
  (:documentation "Get the element type attached to the tracer-file"))
(defmethod get-file-tracer-element-type ((ext t))
  (declare (ignore ext))
  'character)

(defmacro with-tracer ((var &key path class args) &body body)
  "Creates and initialize the appropriate tracer given the extension of PATH."
  (let ((f (gensym))
        (ext (gensym))
        (p (gensym)))
    (cond
      (path
       `(let* ((,p ,path)
              (,ext (pathname-type ,p)))
          (with-open-file (,f ,p :direction :output :if-exists :supersede :element-type (get-file-tracer-element-type (get-file-tracer-for-ext ,ext)) )
            (let ((,var (make-instance (get-file-tracer-for-ext ,ext) :path ,p :file ,f ,@args)))
              (tracer/oncreate ,var)
              (unwind-protect
                   (progn ,@body)
                (tracer/ondestroy ,var))))))
      (class
       `(let ((,p ,class))
            (let* ((,var (make-instance ,p)))
              (tracer/oncreate ,var)
              (unwind-protect
                   (progn ,@body)
                (tracer/ondestroy ,var))))))))

;; PP
(define-file-tracer :pp (tracer-pp)
  ()
  (:documentation "Serialize each signal change to a file"))
(defmethod get-file-tracer-element-type ((ext (eql 'file-tracer-pp)))
  '(unsigned-byte 8))
(defmethod tracer/update ((tracer file-tracer-pp) time key value)
  (conspack:encode (list key time value) :stream (slot-value tracer 'file))
  (force-output (slot-value tracer 'file)))
(defun map-load-pp (func key path)
  "Easy access function for .pp files. Apply FUNC to the time and value of each datapoint matching the KEY"
  (with-open-file (file path :direction :input :element-type '(unsigned-byte 8))
    (loop for entry = (ignore-errors (conspack:decode-stream file))
          while entry
          when (and (listp entry) (string= key (first entry)))
          do (funcall func (second entry) (third entry)))))
(defmethod tracer-pp/get (key (tracer file-tracer-pp))
  ;; TODO Add optimisation to check if the entry has changed before rereading
  (let ((entry (gethash key (tracer-pp/vector-map tracer))))
    (pp-entry/clear entry)
    (map-load-pp (lambda (time value) (pp-entry/push entry time value))
                 key
                 (slot-value tracer 'path))
    entry))

;; CSV
(define-file-tracer :csv (tracer-value)
  ()
  (:documentation "Produce a CSV value. Each column is an entry in the file."))
(defun file-tracer-csv/line (tracer line)
  "Write the contents of the list LINE to the TRACER file."
  (file-tracer/format tracer "~&~{~A~^, ~}" line))
(defmethod tracer/prepare ((tracer file-tracer-csv))
  "Write the header of the csv to the file"
  (file-tracer-csv/line tracer (cons "Time" (hash-table-keys (slot-value tracer 'value-map)))))
(defmethod tracer/draw ((tracer file-tracer-csv) time)
  "Write the next line of the csv to the file"
  (file-tracer-csv/line tracer (cons (datatype-format 'real time)
                                     (loop for key in (hash-table-keys (slot-value tracer 'value-map))
                                           for typ = (gethash key (slot-value tracer 'type-map))
                                           for val = (gethash key (slot-value tracer 'value-map))
                                           collect (datatype-format typ val)))))

;; VCD
(define-file-tracer :vcd ()
  (
   (name-gen-cnt :initform 0
                 :accessor file-tracer-vcd/name-gen-cnt
                 :documentation "Used to generate the vcd names")
   (last-time :initform 0
                 :accessor file-tracer-vcd/last-time
                 :documentation "The last time value written to the vcd file")
   (time-step :initarg :time-step
              :initform -9
              :accessor file-tracer-vcd/time-step
              :documentation "The timescale used in the vcd file as a power of 10.")
   (name-map :initform (make-hash-table :test #'equal) :reader file-tracer-vcd/name-map)
   )
  (:documentation "Produce a VCD file."))
(defun number-to-base (num base)
  "Convert the number to the given base. The output is a list containing the value of each digits"
  (declare (type fixnum num base))
  (nreverse
   (loop for i = num then (floor i base)
         for digit = (mod i base)
         while (> i 0)
         collect digit)))
(defun timestep-to-timename (num)
  (case num
      (-4 "100us")
      (-5 "10us")
      (-6 "1us")
      (-7 "100ns")
      (-8 "10ns")
      (-9 "1ns")
      (-10 "100ps")
      (-11 "10ps")
      (-12 "1ps")
      (t (error "Not implemented timescale"))))
(defun file-tracer-vcd/next-name (tracer)
  "Generate a unique valid vcd name."
  (declare (type file-tracer-vcd tracer))
  (let* ((ix (incf (file-tracer-vcd/name-gen-cnt tracer)))
         (char-ids (number-to-base ix 94)))
    (map 'string #'code-char (mapcar #'(lambda (x) (+ x 33)) char-ids))))
(defmethod tracer/prepare ((tracer file-tracer-vcd))
  "Write the header of the vcd to the file, along with the variable declarations."
  (call-next-method)
  (file-tracer/format tracer "~&$date ~A $end
$version CL-SIMF $end
$timescale ~A $end
$scope module TOP $end "
                      ;; TODO Date
                      "DATE"
                      (timestep-to-timename (file-tracer-vcd/time-step tracer)))
  (loop for k being the hash-keys in (file-tracer-vcd/name-map tracer)
        using (hash-value v)
        for typ = (gethash k (tracer/type-map tracer))
        do
           (multiple-value-bind (vt vw) (datatype-vcd-type typ)
             (file-tracer/format tracer "~& $var ~A ~A ~A ~A $end"
                                 vt
                                 vw
                                 v
                                 k)))
  ;; Initial values
  (file-tracer/format tracer "~&$upscope $end~&$dumpvars")
  (loop for k being the hash-keys in (tracer/type-map tracer)
        using (hash-value typ)
        do
           (file-tracer/format tracer "~&~A~A"
                               (datatype-vcd-format typ (default-value typ))
                               (gethash k (file-tracer-vcd/name-map tracer)))
        )
  (file-tracer/format tracer "~&end")
  )
(defmethod tracer/register ((tracer file-tracer-vcd) (key string) type)
  "Add the KEY/TYPE pair to the TRACER name-map."
  (call-next-method)
  (setf (gethash key (slot-value tracer 'name-map)) (file-tracer-vcd/next-name tracer)))
(defmethod tracer/update ((tracer file-tracer-vcd) time (key string) value)
  (call-next-method)
  (unless (= time (file-tracer-vcd/last-time tracer))
    (file-tracer/format tracer "~&#~A" (floor time (expt 10 (file-tracer-vcd/time-step tracer)))))
  (let ((typ (gethash key (tracer/type-map tracer)))
        (id (gethash key (file-tracer-vcd/name-map tracer))))
    (file-tracer/format tracer "~&~A~A"
                        (datatype-vcd-format typ value)
                        id))
  (setf (file-tracer-vcd/last-time tracer) time))
