
(in-package :cl-user)

;; The device being tested
(f:defmodule sine-generator
  ( (sine :out (f:signal-dir float) ) )
  ((setf $sine (sin (f::element/time f::el)))))

(f:defmodule cosine-generator
  ( (cosine :out (f:signal-dir float) ) )
  ((setf $cosine (cos (f::element/time f::el)))))

(f:defmodule product
    ( (A :in (f:signal-dir float) )
      (B :in (f:signal-dir float) )
      (O :out (f:signal-dir float) ) )
  ((setf $O (* $A $B))))

;; This example shows the use of the postprocessing tracer to plot signals
(defparameter *pp*
  (f:with-simulation
      (env 'f:environment :dt 0.001)
      (tf :class 'f:tracer-pp)
      ((sin "sin" ('f:signal-dir 'float) :trace t)
       (cos "cos" ('f:signal-dir 'float) :trace t)
       (sincos "sin*cos" ('f:signal-dir 'float) :trace t))
      ((singen 'sine-generator
               ((:sine :out sin)))
       (cosgen 'cosine-generator
               ((:cosine :out cos)))
       (prod 'product
             ((:A :in sin)
              (:B :in cos)
              (:O :out sincos))))
    (f:environment/step env :time 10)
    ;; Return the postprocesser
    (f::element-tracer/tracer tf)))

;; Example of using vgplot to plot results
(ql:quickload "vgplot")
(apply #'vgplot:plot
       (mapcan #'(lambda (key) (list (f:pp-entry/time (f:tracer-pp/get key *pp*))
                                     (f:pp-entry/value (f:tracer-pp/get key *pp*))
                                     key) )
               (alexandria:hash-table-keys (f:tracer/type-map *pp*))))
