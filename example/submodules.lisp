(in-package :cl-user)

(f:defmodule register
    ((rst :in (f:signal-dir bit))
     (clk :in (f:signal-dir bit))
     (d :in (f:signal-dir bit))
     (q :out (f:signal-dir bit)))
  ((f:always ((posedge clk))
     (if (= $rst 1)
         (setf $q 0)
         (setf $q $d)))))

(f:defsubmodule delay
    ((rst :in (f:signal-dir bit))
     (clk :in (f:signal-dir bit))
     (input :in (f:signal-dir bit))
     (output :out (f:signal-dir bit)))
  ()
  ()
  ((reg1 'register ((:rst :in rst)
                    (:clk :in clk)
                    (:d :in input)
                    (:q :out :s1)))
   (reg2 'register ((:rst :in rst)
                    (:clk :in clk)
                    (:d :in :s1)
                    (:q :out :s2)))
   (reg3 'register ((:rst :in rst)
                    (:clk :in clk)
                    (:d :in :s2)
                    (:q :out :s3)))
   (reg4 'register ((:rst :in rst)
                    (:clk :in clk)
                    (:d :in :s3)
                    (:q :raw output)))))

(defun run ()
    (f-gui:run-simulation
        (env 'f:environment :dt 0.5)
        (tf :path "/tmp/submodules.pp")
        ( (rst "reset" ('f:signal-dir 'bit) :bind :out :trace t)
          (clk "clock" ('f:signal-dir 'bit) :trace t)
          (input "in" ('f:signal-dir 'bit) :trace t :bind :out)
          (output "out" ('f:signal-dir 'bit) :trace t) )
        ( (clock 'f:clock ((:clk :out clk)) :period 1)
          (dut 'delay
               ((:clk :in clk)
                (:input :in input)
                (:output :out output)
                (:rst :in rst))))
      (setf $rst 1)
      (setf $input 0)
      (f:environment/step env :time 10)
      (setf $rst 0)
      (f:environment/step env :time 10)
      (setf $input 1)
      (f:environment/step env :time 3)
      (setf $input 0)
      (f:environment/step env :time 10)))
