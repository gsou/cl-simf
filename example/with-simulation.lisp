
(in-package :cl-user)

;; The device being tested
(f:defmodule and-gate
  ( (A :in '(signal-dir bit) )
    (B :in '(signal-dir bit) )
    (O :out '(signal-dir bit) ) )
  ((f:always (A B) (setf $O (logand $A $B))))
  (:documentation "A simple module that acts like an AND gate"))

;; This example is the same as manual-simulation, but shows the use of the with-simulation macro
(f:with-simulation
    (env 'f:environment :dt 1)
    (tf :path "/tmp/with-simulation.csv")
    ((a "signal-A" ('f:signal-dir 'bit) :bind :out :trace t)
     (b "signal-B" ('f:signal-dir 'bit) :bind :out :trace t)
     (o "signal-O" ('f:signal-dir 'bit) :trace t :value nil))
    ((dut 'and-gate ((:A :in a)
                     (:B :in b)
                     (:O :out o))))
  (setf $A 0 $B 0) (f:environment/step env)
  (setf $A 0 $B 1) (f:environment/step env)
  (setf $A 1 $B 0) (f:environment/step env)
  (setf $A 1 $B 1) (f:environment/step env))
