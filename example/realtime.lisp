(in-package :cl-user)

(defun run ()
  (f-gui:run-simulation
      (env 'f:environment-rt)
      (tf :path "/tmp/rt.pp")
      ((sin-value "sin-value" ('f:signal-dir t) :trace t :value 0.0d0)
       (cos-value "cos-value" ('f:signal-dir t) :trace t :value 0.0d0)
       )
      ((cos 'f::generator ((:value :out cos-value)) :function #'cos)
       (int 'f::integral ((:i :in cos-value) (:o :out sin-value))))))
