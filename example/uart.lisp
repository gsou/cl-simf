(in-package :cl-user)

(f:defmodule uart-logger
  ((uart :mess (f:signal-message fixnum)))
  ((f:on-message (m uart)
     (format t "~A" (code-char m))
     (finish-output)))
  (:documentation "Print the received data to stdout"))
(defmethod send-string-uart ((log uart-logger) text)
  (loop for i across text
        do (f:interface-signal/write (slot-value log 'uart) (char-code i))))


(defun run ()
    (f:with-simulation
        (env 'f:environment :dt 1d-9)
        (tf :path "/tmp/uart.vcd" :args (:time-step -9))
        ()
        ((log 'uart-logger ((:uart :mess :uart)))
         (per 'f:uart ((:uart :mess :uart)
                       (:clk :in :clk)
                       (:tx :out :txrx)
                       (:rx :in :txrx)))
         (clock 'f:clock ((:clk :out :clk)) :period 8680.556d-9))

      (f:environment/step env :time 10d-6)

      (send-string-uart log
                       "Lorem ipsum dolor sit amet, consectetur adipiscing elit.")

      (f:environment/step env :time 8000d-6)))
