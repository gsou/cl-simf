
;; This is an example of a simulation of a PMSM motor
;; using a currently unreleased motor controller model "inverter"

(in-package :cl-user)

(f:defmodule-class pilot-brake ()
  ( (speed :in (f:signal-dir float))
    (brake :out (f:signal-dir float)) )
  ( (state :initform :wait :accessor pilot-brake/state) )
  ((case (pilot-brake/state f::el)
     (:wait
      (setf $brake 0d0)
      (when (> (f::element/time f::el) 4)
        (set-pedal 0)
        (setf (pilot-brake/state f::el) :brake)
        (setf $brake -5.0)
        ))
     (:brake (when (< $speed 3000)
               (set-pedal 32767)
               (setf $brake 0)
               (setf (pilot-brake/state f::el) :stop)))
     (:stop nil)
     (t (error "State machine error")))))


(ql:quickload "cl-random")
(defparameter *pp* nil)
(defun run-motor (steps step-width &key (down steps) (dt 11.55d-6))
  (f:with-simulation
      (env 'f:environment :dt dt)
      (tf :path "/tmp/motorsimoutput.vcd" :args (:time-step -8)
          )
      ((clk "clock" ('f:signal-dir 'bit) :trace nil)

       (angle "angle" ('f:signal-dir 'float) :trace t)
       (theta-obs "theta-obs" ('f:signal-dir 'float) :trace t)
       (theta-comp "theta-comp" ('f:signal-dir 'float) :trace t)
       (theta-enc "theta-enc" ('f:signal-dir 'float) :trace t)
       (encoder "encoder" ('f:signal-dir 'float) :trace t)
       (speed "speed" ('f:signal-dir 'float) :trace t)
       (torque "torque" ('f:signal-dir 'float) :trace t)

       (load "load" ('f:signal-dir 'float) :trace t)
       (friction "friction" ('f:signal-dir 'float) :trace nil)
       (brake "brake" ('f:signal-dir 'float) :trace nil)
       (load-ref "load-ref" ('f:signal-dir 'float) :trace nil)

       (ia "ia" ('f:signal-dir 'float) :trace t)
       (sensor-ia "sensor-ia" ('f:signal-dir 'float) :trace t)
       (ib "ib" ('f:signal-dir 'float) :trace t)
       (sensor-ib "sensor-ib" ('f:signal-dir 'float) :trace t)
       (ic "ic" ('f:signal-dir 'float) :trace t)
       (sensor-ic "sensor-ic" ('f:signal-dir 'float) :trace t)

       ;; Observer
       (is-est-a "is-est-a" ('f:signal-dir 'float) :trace t)
       (is-est-b "is-est-b" ('f:signal-dir 'float) :trace t)
       (z-a "z-a" ('f:signal-dir 'float) :trace t)
       (z-b "z-b" ('f:signal-dir 'float) :trace t)
       (e-est-a "e-est-a" ('f:signal-dir 'float) :trace nil)
       (e-est-b "e-est-b" ('f:signal-dir 'float) :trace nil)
       (e-est-f-a "e-est-f-a" ('f:signal-dir 'float) :trace t)
       (e-est-f-b "e-est-f-b" ('f:signal-dir 'float) :trace t)

       (mspeed "measured-speed" ('f:signal-dir 'float) :trace t)
       (speedlim-spiq "speed-limiter.spiq" ('f:signal-dir 'float) :trace t)
       (speedlim-int "speed-limiter.int" ('f:signal-dir 'float) :trace t)

       (spiq "spiq" ('f:signal-dir 'float) :trace t)

       (id "id" ('f:signal-dir 'float) :trace t)
       (iq "iq" ('f:signal-dir 'float) :trace t)
       (ialpha "ialpha" ('f:signal-dir 'float) :trace t)
       (ibeta "ibeta" ('f:signal-dir 'float) :trace t)

       (va "va" ('f:signal-dir 'float) :trace nil)
       (vb "vb" ('f:signal-dir 'float) :trace nil)
       (vc "vc" ('f:signal-dir 'float) :trace nil))
      ((inv 'inverter ((:clk :in clk)
                       ;; (:ia :in ia)
                       ;; (:ib :in ib)
                       ;; (:ic :in ic)
                       (:i-alpha :out ialpha)
                       (:i-beta :out ibeta)
                       (:ia :in sensor-ia)
                       (:ib :in sensor-ib)
                       (:ic :in sensor-ic)
                       (:spiq :out spiq)
                       ;; (:theta :in angle)
                       (:theta :in encoder)
                       (:theta-obs :out theta-obs)
                       (:theta-comp :out theta-comp)
                       (:theta-enc :out theta-enc)
                       (:is-est-a :out is-est-a )
                       (:is-est-b :out is-est-b )
                       (:z-a :out z-a )
                       (:z-b :out z-b )
                       (:e-est-a :out e-est-a )
                       (:e-est-b :out e-est-b )
                       (:e-est-f-a :out e-est-f-a )
                       (:e-est-f-b :out e-est-f-b )
                       (:speed :out mspeed)
                       (:speed-lim-spiq :out speedlim-spiq)
                       (:speed-int :out speedlim-int)
                       (:va :out va)
                       (:vb :out vb)
                       (:vc :out vc)))

       (variable-load 'f::generator ((:value :out load-ref))
                      :function (lambda (time)
                                  (+ (* 2 (sin time))
                                     (cl-random:from-standard-normal
                                      (cl-random:draw-standard-normal) 5 2))))
       (motor-load 'cl-simf-models::load-res
                   ((:load :out friction)
                    (:speed :in speed)
                    (:input :in load-ref)))
       (pilot 'pilot-brake
              ((:speed :in speed)
               (:brake :out brake)))
       (comb-load 'f:comb2
                  ((:in1 :in brake)
                   (:in2 :in friction)
                   (:value :out load))
                  :function #'+)

       (encoder 'f:comb1
                ((:in :in angle)
                 (:value :out encoder))
                :time t
                :function
                #'(lambda (time angle)
                              (mod
                               (+ (* 0.10 (sin (* 1000 2 pi time)))
                                  (* 0.05 (sin (* 2000 2 pi time)))
                                  (* 0.02 (sin (* 3000 2 pi time)))
                                  (* 0.01 (sin (* 4000 2 pi time)))
                                  angle
                                  (cl-random:from-standard-normal
                                   (cl-random:draw-standard-normal) 0 0.01))
                               (* 2 pi))))

       (current-sensor-a 'f:comb1
                         ((:in :in ia)
                          (:value :out sensor-ia))
                         :time t
                         :function
                #'(lambda (time i)
                               (+ (* 1.00 (sin (* 1000 2 pi time)))
                                  (* 0.50 (sin (* 2000 2 pi time)))
                                  (* 0.20 (sin (* 3000 2 pi time)))
                                  (* 0.20 (sin (* 4000 2 pi time)))
                                  i
                                  (cl-random:from-standard-normal
                                   (cl-random:draw-standard-normal) 0 10))))
       (current-sensor-b 'f:comb1
                         ((:in :in ib)
                          (:value :out sensor-ib))
                         :time t
                         :function
                #'(lambda (time i)
                               (+ (* 1.00 (sin (* 1000 2 pi time)))
                                  (* 0.50 (sin (* 2000 2 pi time)))
                                  (* 0.20 (sin (* 3000 2 pi time)))
                                  (* 0.20 (sin (* 4000 2 pi time)))
                                  i
                                  (cl-random:from-standard-normal
                                   (cl-random:draw-standard-normal) 0 10))))
       (current-sensor-c 'f:comb1
                         ((:in :in ic)
                          (:value :out sensor-ic))
                         :time t
                         :function
                #'(lambda (time i)
                               (+ (* 1.00 (sin (* 1000 2 pi time)))
                                  (* 0.50 (sin (* 2000 2 pi time)))
                                  (* 0.20 (sin (* 3000 2 pi time)))
                                  (* 0.20 (sin (* 4000 2 pi time)))
                                  i
                                  (cl-random:from-standard-normal
                                   (cl-random:draw-standard-normal) 0 10))))


       (mot 'cl-simf-models::pmsm
            ((:angle :out angle)
             (:speed :out speed)
             (:torque :out torque)
             (:ia :out ia)
             (:ib :out ib)
             (:ic :out ic)
             (:id :out id)
             (:iq :out iq)
             (:load :in load)
             (:va :in va)
             (:vb :in vb)
             (:vc :in vc))
            :kt 0.1
            :kv (/ 0.10 4)
            :r 0.02
            :l 7.2e-5
            :j 0.005
            :p 4)
       (clock 'f:clock ((:clk :out clk))
              :period 46.2e-6))

    ;; Trace params
    ;; (setf (f::element-tracer/downsample tf) 16)
    ;; (setf (f::element-tracer/continuous tf) t)

    ;; Run simulation
    (init-inverter)
    (set-pedal 0)
    (f:environment/step env :time step-width)
    (set-pedal 32767)
    (loop for ix below steps
          do (progn
               (format t "~&Ramp up: ~A out of ~A" ix steps)
               (f:environment/step env :time step-width)))
    (set-pedal 0)
    (loop for ix below down
          do (progn
               (format t "~&Ramp down: ~A out of ~A" ix down)
               (f:environment/step env :time step-width)))

    (format t "~&Simulation done")
    (f::element-tracer/tracer tf)))


;; (run-motor 30 0.5 :down 5)
