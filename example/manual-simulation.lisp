
(in-package :cl-user)

;; The device being tested
(f:defmodule and-gate
  ( (A :in (f:signal-dir bit) )
    (B :in (f:signal-dir bit) )
    (O :out (f:signal-dir bit) ) )
  ((f:always (A B) (setf $O (logand $A $B))))
  (:documentation "A simple module that acts like an AND gate"))

;; Create and run the simulation manually

;; Create the environment
(let ((env (make-instance 'f:environment :dt 1)))
  ;; Create a tracer element to log the values
  (f:with-element-tracer (tf :path "/tmp/manual-simulation.vcd") env
    (let* (;; The signals
           (signal-A (make-instance 'f:signal-dir :name "signal-A" :type 'bit :value nil))
           (signal-B (make-instance 'f:signal-dir :name "signal-B" :type 'bit :value nil))
           (signal-O (make-instance 'f:signal-dir :name "signal-O" :type 'bit :value nil))

           ;; Writers for the input signals
           (iface-A (f:signal/bind-out signal-A))
           (iface-B (f:signal/bind-out signal-B))

           ;; Create DUT
           (dut (make-instance 'and-gate
                               :env env
                               :A (f:signal/bind-in signal-A)
                               :B (f:signal/bind-in signal-B)
                               :O (f:signal/bind-out signal-O))))

      (f:interface-signal/eventp iface-A :changed)

      ;; Registers the modules to the environment
      (f:environment/register-all env tf dut)

      ;; Register signals to tracer
      (f:element-tracer/add tf (f:signal/bind-in signal-A))
      (f:element-tracer/add tf (f:signal/bind-in signal-B))
      (f:element-tracer/add tf (f:signal/bind-in signal-O))


      ;; Initialize the variables
      (f:interface-signal/write iface-A 0)
      (f:interface-signal/write iface-B 0)
      (f:environment/step env)

      ;; Try all combs...
      (f:interface-signal/write iface-A 0)
      (f:interface-signal/write iface-B 1)
      (f:environment/step env)

      (f:interface-signal/write iface-A 1)
      (f:interface-signal/write iface-B 0)
      (f:environment/step env)

      (f:interface-signal/write iface-A 1)
      (f:interface-signal/write iface-B 1)
      (f:environment/step env))))
