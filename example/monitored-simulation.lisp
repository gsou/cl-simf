(in-package :cl-user)

(f:defmodule counter
  ( (clk :in (f:signal-dir bit))
    (rst :in (f:signal-dir bit))
    (cnt :out (f:signal-dir integer)) )
  ((f:always ((posedge clk)) (if (= $rst 0) (incf $cnt) (setf $cnt 0))))
  (:documentation "Count up everytime the clock ticks"))

(setf *monitor* (f:with-simulation-monitor
                  (env 'f:environment :dt 0.5)
                  (tf :class 'f:tracer-pp)
                  ( (rst "reset" ('f:signal-dir 'bit) :bind :out :trace t)
                    (clk "clock" ('f:signal-dir 'bit) :trace t)
                    (count "count" ('f:signal-dir 'integer) :trace t) )
                  ( (clock 'f:clock ((:clk :out clk)) :period 1)
                    (dut 'counter ((:clk :in clk)
                                   (:rst :in rst)
                                   (:cnt :out count))) )

                (f:environment/step env :time 10)
                (setf $rst 1)
                (f:environment/step env :time 3)
                (setf $rst 0)
                (f:environment/step env :time 10)))
