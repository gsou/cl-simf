;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.

(asdf:defsystem #:cl-simf
  :description "Simulation framework"
  :version "0.1"
  :depends-on ("alexandria" "jpl-queues" "cl-ppcre" "parse-float" "trivia" "cl-conspack" "str")
  :author "gsou"
  :license "GPL"
  :pathname "src/"
  :serial t
  :components ((:file "packages")
               (:file "datatypes")
               (:file "tracing")
               (:file "signal")
               (:file "element")
               (:file "simulation")
               (:file "submodule")
               (:file "definition")
               (:module "modules"
                :pathname "modules/"
                :serial t
                :components
                ((:file "clock")
                 (:file "replay-candump")
                 (:file "serial")
                 (:file "generator")))))

(asdf:defsystem #:cl-simf/models
  :description "Models for cl-simf"
  :version "0.1"
  :depends-on ("alexandria" "cl-simf" "cl-protobufs")
  :author "gsou"
  :license "GPL"
  :pathname "models/"
  :serial t
  :components ((:file "packages")
               (:file "loads")
               (:file "pmsm")
               (:file "rics-protobuf")
               (:file "rics")))
