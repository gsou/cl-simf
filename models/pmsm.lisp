;; Copyright (C) 2021-2022 gsou
;;
;; This file is part of cl-simf.
;;
;; cl-simf is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; cl-simf is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with cl-simf.  If not, see <https://www.gnu.org/licenses/>.
;;
;; Very simple example model of a PMSM for cl-simf

(in-package :cl-simf-models)

(defmodule-class pmsm ()
  (;; STATE
   (angle :out '(signal-dir float)) ;; Angle of the rotor in radians
   (speed :out '(signal-dir float)) ;; Speed of the rotor in rpm
   (torque :out '(signal-dir float)) ;; Torqueof the rotor in Nm
   ;; Electical
   (ia :out '(signal-dir float)) ;; Phase currents
   (ib :out '(signal-dir float))
   (ic :out '(signal-dir float))
   (id :out '(signal-dir float))
   (iq :out '(signal-dir float))
   ;; CTRL
   (load :in '(signal-dir float)) ;; External torque applied to the motor
   (va :in '(signal-dir float)) ;; Phase voltages
   (vb :in '(signal-dir float))
   (vc :in '(signal-dir float)))
  ((param-kt :initarg :kt :accessor pmsm/kt) ;; Torque output [Nm/A]
   (param-kv :initarg :kv :accessor pmsm/kv) ;; [Wb]
   (param-r :initarg :r :accessor pmsm/r) ;; Phase resistance [Ohm]
   (param-l :initarg :l :accessor pmsm/l) ;; Phase inductance (Q and D) [Ohm]
   (param-j :initarg :j :accessor pmsm/j) ;; Total inertia [kg m^2]
   (param-p :initarg :p :accessor pmsm/p) ;; Pole pairs
   (int-id-tmp :initform 0.0 :accessor pmsm/int-id-tmp)
   (int-id :initform 0.0 :accessor pmsm/int-id)
   (int-iq :initform 0.0 :accessor pmsm/int-iq)
   (int-speed :initform 0.0 :accessor pmsm/int-speed)
   )
  ((let* ((dt (f::element/dt f::el))
          (avg (/ (+ $va $vb $vc) 3))
          (va-avg (- $va avg))
          (vb-avg (- $vb avg))
          (vc-avg (- $vc avg))
          (pos-elc (* (pmsm/p f::el) $angle))
          (sqrt2/3 (sqrt (/ 2.0 3.0)))
          (int-vd (* sqrt2/3
                     (+ (* va-avg (cos pos-elc))
                        (* vb-avg (cos (- pos-elc (* 2/3 pi))))
                        (* vc-avg (cos (+ pos-elc (* 2/3 pi)))))))
          (int-vq (* sqrt2/3
                     (- 0
                        (* va-avg (sin pos-elc))
                        (* vb-avg (sin (- pos-elc (* 2/3 pi))))
                        (* vc-avg (sin (+ pos-elc (* 2/3 pi)))))))
          (L (pmsm/l f::el))
          (R (pmsm/r f::el))
          (p (pmsm/p f::el))
          (kv (pmsm/kv f::el))
          (int-speed (pmsm/int-speed f::el))
          )
     (incf (pmsm/int-id-tmp f::el) (/ (* dt (+ int-vd
                                               (* p int-speed L (pmsm/int-iq f::el))
                                               (- (* R (pmsm/int-id f::el)))))
                                      L))
     (setf (pmsm/int-id f::el) (pmsm/int-id-tmp f::el))
     (setf $id (pmsm/int-id f::el))
     (incf (pmsm/int-iq f::el)
           (/ (* dt (- int-vq
                       (* int-speed p (+ kv (* l (pmsm/int-id f::el))))
                       (* (pmsm/int-iq f::el) r)))
              L))
     (setf $iq (pmsm/int-iq f::el))

     (let* ((int-id (pmsm/int-id f::el))
            (int-iq (pmsm/int-iq f::el))
            (kt (pmsm/kt f::el))
            (tau-m (* kt (pmsm/int-iq f::el)))
            (tau-tot (+ tau-m $load))
            (j (pmsm/j f::el)))
       (incf (pmsm/int-speed f::el) (/ (* tau-tot dt) j))
       (setf $angle (mod (+ $angle (* int-speed dt)) (* 2 pi)))
       (setf $speed (* 9.54592965964254 int-speed))
       (setf $ia (* sqrt2/3 (- (* int-id (cos pos-elc))
                               (* int-iq (sin pos-elc)))))
       (setf $ib (* sqrt2/3 (- (* int-id (cos (- pos-elc (* 2/3 pi))))
                               (* int-iq (sin (- pos-elc (* 2/3 pi)))))))
       (setf $ic (* sqrt2/3 (- (* int-id (cos (+ pos-elc (* 2/3 pi))))
                               (* int-iq (sin (+ pos-elc (* 2/3 pi)))))))
       (setf $torque tau-tot))))
  (:documentation "Very simple model of a PMSM."))
